---
layout: markdown_page
title: "Command of the Message"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Force Management's definition of Command of the Message is "being audible ready to define your solutions to customers’ problems in a way that differentiates you from your competitors & allows you to charge a premium for your products & services.” Critical sales skills to demonstrate Command of the Message include:
*  Uncover Customer Needs
*  Articulate Value and Differentiation
*  Negotiate Value

## Command of the Message Mantra
The Mantra is a framework you can use to clearly demonstrate that you have a complete understanding of your customer's goals, needs, and metrics for success. It also provides you with a customer-focused context to transition from the customer's needs which you have clearly articulated into how GitLab helps meet those needs. The Mantra is also a very good meeting prepartion tool to determine how well you know you're customer. If you cannot clearly articulate a customer-specific mantra, then you are not ready to progress the deal and you need to do more research. Below is a breakown of the CoM Mantra framework which can be adjusted and articulated in your own words.

| **COM Mantra** | **Description** |
| ------ | ------ |
| What I hear you saying Mr./Ms. Customer is that these are the **Positive Business Outcomes** you’re trying to achieve…| Have you clearly capture your customer's business goals?|
| In order to achieve these positive business outcomes, we agreed that these are the **Required Capabilities** you’re going to need..| Have you clearly captured and documented the customer's requiremet and aligned them with Gitlab's products and features? |
| And you’ll probably want to measure these required capabilities using these **Metrics** | Have you worked with the customer to determine what metrics will measure success? |
| Let me tell you **How We Do It…** | Can you articulate how the GitLab product works? |
| Let me tell you **How We Do It Better/Differently…** | Did you research and align GitLab's Defensible Differentiators witih the customer's needs? |
| But don’t take my word for it…**(Proof Points)** | Can you offer tangible evidence that you’ve been able to satisfy the Required Capabilitie? |

## Defensible Differentiators

1.  Single Application for Entire DevOps Lifecycle
1.  Leading SCM and CI in One Application
1.  Built In Security and Compliance
1.  Deploy Your Software Anywhere
1.  Optimized for Kubernetes
1.  End-to-End Insight and Visibility
1.  Flexible GitLab Hosting Options
1.  Rapid Innovation
1.  Open Source; Everyone Can Contribute
1.  Collaborative and Transparent Customer Experience

## Resources: Core Content

| **Asset Title** | **Why / When Use** |
| ------ | ------ |
| [GitLab Value Framework](https://drive.google.com/open?id=1E0QM_dNhAo9bslmtTW1xGqnT9E3kbHBt7UhwGtdYIV8) | primary asset with in-depth information on GitLab's value drivers and defensible differentiators |
| [GitLab Value Framework Summary](https://drive.google.com/open?id=1l2g7OJ3mIrCgUlLvwQbtwo-2Eg1lV8rkqUTy9dEoLdc) | 1-page quick guide, also great for sharing with internal champions |
| [GitLab CXO Digital Transformation Discovery Guide](https://drive.google.com/open?id=1balLINV-vnd6-6TYzF3SIIJKrBPUQofVSDgQ5llC2Do) | 3-page conversation guide for executive discussions |
| [Proof Points](/handbook/sales/command-of-the-message/proof-points.html) | don't take our word for it--3rd party validation from customers, analysts, industry awards, and peer reviews |
| [NEW Customer deck based on GitLab value drivers](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing) | alternative to the standard GitLab pitch deck that starts with the customer's perspective and aligns to the 3 value drivers. Video [here](https://youtu.be/UdaOZ9vvgXM) |

## Command Plan

*  [Opportunity Management Requirements](https://about.gitlab.com/handbook/sales/#opportunity-management-requirements)
*  Command Plan templates
   - [Blank template](https://drive.google.com/open?id=1uTHRRUQx4IP_dXUnsrfG2x9Ti-XpMneX3IDu2v0TZtY)
   - [Template with helper text](https://drive.google.com/open?id=1SH7yfGFda0jsHcM9QmVI9BQO54fuaMujt-aJfFg-EUA)
   - [Sample Command Plan](https://drive.google.com/open?id=1SdfgEds7NvCezcrFjirdLygIp4715iQn7u2ts-9avwg)

## Resources: Additional Job Aids

*  Overview resources
   - Comprehensive [GitLab CoM & MEDDPICC training slide deck](https://drive.google.com/open?id=1bWdV__GwN9WzkidBc0qMFu1GGln3rf5C)
   - [GitLab CoM & MEDDPICC Participant Guide](https://drive.google.com/open?id=1qSn-PZJ9_mk-dhnRY01BdoeBcrtC7jVr)
*  Prepare
   - [Pre-Call Plan](https://docs.google.com/document/d/1yjyfvMoDvayZca5hXiIwSHYc9T1M3mTc7ocqzjhqOf8/copy)
*  Discovery
   - [Customer Call Notes Template](https://docs.google.com/document/d/1hlLvfgQMgQS5g2ykEc6eNZP_wZd1M8GSmS-JsN_vICU/copy)
      - You may also choose to utilize the [Role Play Notes](https://docs.google.com/document/d/185a4mI3HMFnV_l6NwrsAndduFFlTvm5tPiIuPVy0ONQ/copy) template
*  Qualify
   - [Capturing "MEDDPICC" Questions for Deeper Qualification](/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification)
   - [MEDDPICC training slides](https://drive.google.com/open?id=1i3D64esfBitwn1ZXKB1-yjs52Z5hMsUggVClUKTcqjk)
   - [MEDDPICC template](https://docs.google.com/document/d/1WbHoSL4r7S553n90sAEVuSdBNImWfCk3vTJINw2ud8A/copy)
   - [Opportunity Qualifier](https://docs.google.com/document/d/1Tz6bQKD4Ff2-XqpSXRQslD8yvrphwXaL6oEl74DAjeQ/copy)
*  Role Play materials
   - [Role Play Prep Sheet](https://docs.google.com/document/d/1nQ2yH4hg_btFi5XGHhvDjNh9-TKgxAYGO-bLYl8cMdc/copy)
   - [Role Play Notes](https://docs.google.com/document/d/185a4mI3HMFnV_l6NwrsAndduFFlTvm5tPiIuPVy0ONQ/copy)
*  Sales Manager materials
   - [Opportunity Coaching Guide](https://docs.google.com/document/d/1IZA9Fo2SvZOrtUVpXOjwwqs76lKdXFs4hTezbxRq5v8/copy)
   - [GitLab Manager Coaching slide deck](https://drive.google.com/open?id=1xxWlYd-YoRa51B5AD1LAdl3x5DsXBxfx)
   - [GitLab CoM & MEDDPICC Fast Start Program Manager Playbook](https://drive.google.com/open?id=1n76gU6whKW51ixMfFvgXoGsq512PsCHG)

GitLab sales professionals and Sales Development Reps (SDRs) may access additional information in the [Force Management Command Center](https://gitlab.lyearn.com/) (password protected since resources contain Force Management intellectual property). In particular, the [Channels](https://gitlab.lyearn.com/#/learner/channels) section of the Force Management Command Center contains supplemental instructional videos, podcasts, and blogs).

## Sharing Feedback

Over time, the GitLab Value Framework and associated content above will iterate and evolve. To ensure these changes are easily consumable, iterations will be made on a predictable cadence. We will start with a quarterly cadence. 
*  At the start of the last month of a fiscal quarter, Field Enablement will announce a Call for Feedback to solicit input/feedback on suggested iterations and improvements to the GitLab Value Framework and associated content (but feedback may be submitted at any time)
*  To share feedback, submit an issue using [**this issue template**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/new?issuable_template=value-framework-feedback)  
*  Field Enablement and Product Marketing leadership will review feedback during the middle of the first month of a new quarter. Additional stakeholders and subject matter experts will be pulled in as appropriate.
*  During this review, Field Enablement and Product Marketing will determine the disposition for each issue submitted with three possible outcomes
   1. Accepted (label: `vff::accepted`) - Value Framework feedback that will be actioned on
   1. Deferred (label: `vff::deferred`) - Value Framework feedback that will be deferred until more information is gathered
   1. Declined (label: `vff::declined`) - Value Framework feedback that is declined (no action will be taken)
*  When the `vff::accepted` label is added, a version label will be applied (`ver::1.1`, `ver::1.2`, etc.) to indicate the version of the Value Framework in which the improvement will be implemented
*  When the improvement has been implemented, the `vff::completed` label will be applied 
*  Around the end of the first month of a new quarter, a summarized update of accepted feedback will be shared with the field and implementation of those updates will be tracked in the issues
