---
layout: handbook-page-toc
title: "Handbook Technical Docs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

This page contains technical documentation related to the handbook.

It is currently a placeholder page.  Most of the technical documentation for the handbook is under [the `/doc` directory](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/doc) in markdown files, and not part of the handbook.  There is [an open issue to review, update and surface relevant information from `/doc` into the Handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7234) to 

## CI/CD

### Skipping review app job on MR builds

To skip/bypass the review app deploy job for MRs which do not need it, you can include the string `[SKIP REVIEW]` or `[REVIEW SKIP]` (case insensitive) in the title of the MR.
