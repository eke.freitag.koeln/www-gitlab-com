---
layout: handbook-page-toc
title: "Partner Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who are we

Partner Marketing is the single connection point from alliances and the channel to PMM, PM, FMM, corporate events, digital marketing and marketing programs. We ensure joint partner/channel messaging, joint value proposition and focus on the current and future scope for both organizations. Our main goal is to leverage our strategic partners to increase awareness of GitLab while increasing joint sales for our enterprise users.

<i class="fas fa-users fa-fw color-orange font-awesome" aria-hidden="true"></i> **The Team**

- [**Tina Sturgis**](https://about.gitlab.com/company/team/#TinaS), 
Manager, Partner and Channel Marketing **{Focused on AWS, Channel Marketing interim, Commit partner sponsorships, & overall Partner and Channel Marketing Strategy}**

- [**Alisha Rashidi**](https://about.gitlab.com/company/team/#Arashidi), 
Partner and Channel Marketing Manager **{Focused on Google Cloud, VMware and AWS}**

- [**Sara Davila**](https://about.gitlab.com/company/team/#saraedavila), 
Senior Partner and Channel Marketing Manager **{Focused on Eco-partners, RedHat, Azure & HashiCorp}**

This page will cover all of Technology Partner Marketing. If you need information for Channel Partner Marketing, [click here](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/channel-marketing/). 


## <i class="fab fa-gitlab fa-fw color-orange font-awesome" aria-hidden="true"></i> What We Do 

- Drive awareness, demand generation and influence joint revenue by way of executing GTM programs with [cloud and platform providers, and integration partners](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit?usp=sharing). 
- Evaluates and fills the gaps of joint content and collateral for Tier 1 and ecosystem partners.
- Maximize partner MDF programs to participate and promote partner specific events, webcasts, content initiatives, etc.
- Funnels in income from partner sponsorships for virtual and physical events.

## How to Request Partner Marketing Support
*Partner Marketing executes:*
* Partner hosted webcasts
* GitLab hosted webcasts
* Partner guest blogs
* Updates to partner solution webpages
* Events
* And More

**How To Engage Partner Marketing:**

*Partner Marketing's inital issues response SLA is 48 hours.*
To start the Partner Marketing support process, 
* [Create an issue](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) 
* Apply the label: Partner Marketing

For additional information on how issues are managed, please reference [*Requesting strategic marketing team help/support.*](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport)

