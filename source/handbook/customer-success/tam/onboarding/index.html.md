---
layout: handbook-page-toc
title: "Account Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

# Customer Onboarding

When a customer purchases or upgrades GitLab to an ARR of $50,000 or greater, an [onboarding object](/handbook/customer-success/using-salesforce-within-customer-success/#onboarding-objects) is automatically created.

The Technical Account Management team are subscribed to the [Customer Onboarding Report](https://gitlab.my.salesforce.com/00O4M000004drw7) in Salesforce, emailed every Monday. Use the [Customer Onboarding Dashboard](https://gitlab.my.salesforce.com/01Z4M000000skrQ) for richer analysis.

_Handy tip: In order to delete an onboarding object, you must first make yourself the owner of it._

Once an account has been assigned to a Technical Account Manager, they must open an onboarding issue in the [Customer Onboarding GitLab project](https://gitlab.com/gitlab-com/customer-success/customer-onboarding). [The issue template](https://gitlab.com/gitlab-com/customer-success/customer-onboarding/issues/new?issuable_template=%5BCustomer%5D%20Onboarding&fds) has instructions.

The goal of the issue template is to help Technical Account Managers more consistently deliver a more thorough onboarding experience for customers, so they are set-up for success. If an item in the checklist doesn't make sense for a particular customer, mark it complete and leave a note in the comments.

An overview of these tools is [recorded in this video](https://www.youtube.com/watch?v=JLTvuTSycnQ&feature=youtu.be&hd=1) (Private Gitlab Unfiltered link).

Please also review our [TAM and Support interaction](/handbook/customer-success/tam/support) page as well as our [Support](/handbook/support) handbook section to assist with sharing support information with customers.

## Playbooks
The purpose of a playbook is to build standardized, repeatable and scalable processes. A playbook helps you quality assure the basics of your customer success methodology. All of our playbooks will be stored on our GDrive under `Sales/Customer Success/Playbooks`.
