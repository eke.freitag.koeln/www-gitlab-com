// Define the namespace
var runExperiment = function(issueId, featureFlag, experimentControlExtraFunctions, experimentTestExtraFunctions, featureFlagEnvironment)
{
  // Define variables & configurations
  var consoleLogsEnable = new Boolean(false);
  var experimentWrapperId = '#experiment' + issueId;
  var experimentTargetControl = experimentWrapperId + ' .experiment-control';
  var experimentTargetTest = experimentWrapperId + ' .experiment-test';
  var experimentLoading = experimentWrapperId + ' .loading-experiment';
  var controlExtras = experimentControlExtraFunctions;
  var testExtras = experimentTestExtraFunctions;
  var environmentValue = featureFlagEnvironment;
  var production = '5e4333e30437cc080f58513a';
  var test = '5e4333e30437cc080f585139';
  var dev = '5e4f2535a16fb207f4e766fd';
  var environmentKey;

  // Process the environment configuration
  if(environmentValue === 'production'){environmentKey = production;}
  else if(environmentValue === 'test'){environmentKey = test;}
  else {environmentKey = dev;}
  if(consoleLogsEnable == true)
  {
    console.log('feature flag environment: ' + environmentValue + ', environment id: ' + environmentKey);
  };

  // Initialize the user object
  var user = {'anonymous': true};
  // Initialize LaunchDarkly
  var ldclient = LDClient.initialize(environmentKey, user);

  // Define what should happen
  function render()
  {

    // Init the feature flag object
    var isThisTheTestVariant = ldclient.variation(featureFlag, false);
    if(consoleLogsEnable == true)
    {
      console.log('issue id: ' + issueId + ', flag name: ' + featureFlag + ', isThisTheTestVariant: ' + isThisTheTestVariant);
    };

    // Show, hide, and run extra functions.
    if(isThisTheTestVariant)
    {
      // THIS IS THE TEST VARIANT

      // TIMEOUTS: wait 150 ms to poll isThisTheTestVariant
      setTimeout(function()
      {
        document.querySelector(experimentLoading).classList.add('loaded-experiment');
        document.querySelector(experimentTargetControl).classList.remove('experiment-visible');
        document.querySelector(experimentTargetTest).classList.add('experiment-visible');
      }, 150);

      if(experimentTestExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          testExtras();
          if(consoleLogsEnable == true)
          {
            console.log('test extras: ' + testExtras);
          };
        }, 350);
      };

    } else {
      // THIS IS THE CONTROL VARIANT

      // TIMEOUTS: wait 100 ms to poll isThisTheTestVariant
      setTimeout(function()
      {
        document.querySelector(experimentLoading).classList.add('loaded-experiment');
        document.querySelector(experimentTargetTest).classList.remove('experiment-visible');
        document.querySelector(experimentTargetControl).classList.add('experiment-visible');
      }, 150);

      if(experimentControlExtraFunctions !== null)
      {
        // TIMEOUTS: wait another 200 ms for the fade animation to finish (total 350ms)
        setTimeout(function()
        {
          controlExtras();
          if(consoleLogsEnable == true)
          {
            console.log('control extras: ' + controlExtras);
          };
        }, 350);
      };
    };
  };
  // Subscribe to the feature flag and then run the functions based on it's state
  ldclient.on('ready', function()
  {
    if(consoleLogsEnable == true)
    {
      console.log('feature flag system is ready');
    };
    render();
  });
  /*
  // commented out for now. typically we don't need this to operate on change.
  ldclient.on('change', function()
  {
    if(consoleLogsEnable == true)
    {
      console.log('feature flag "' + featureFlag + '" status has changed');
    };
    render();
  });
  */
};
